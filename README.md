# Gulp Boilerplate
> Tools to optimize the front-end development with Gulp.

## Requirements

-   [Node](https://nodejs.org/en/)
-   [Gulp](https://gulpjs.com/)
-   [NPM](https://www.npmjs.com/)

## Dependencies

```js
npm install 
// To install all project dependencies
```

## Tasks

```js
npm start or gulp
// To start the server 

npm build or gulp build --production 
// To generate all production files 

npm generate or gulp assets
// To generate only assets files
```
